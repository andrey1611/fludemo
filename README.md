**Assumptions:**
1) User should be added to sudoers and have sudo permission
2) Automatization process was developed especially for virtual servers provided by http://vscale.io
3) Tested on Debian 9 and Debian 10 64bit. 
4) Minimum server configuration: 1 Gb RAM 30 Gb SSD 1 CPU

**High-level description:**

The system consist of 
- flussonic server (stream video, record archive)
- portal (tv schedule, watching live stream, video records)

Portal based on the next technologies:
- PHP + Laravel + sqlite
- nginx + php-fpm
- docker + docker-compose
- TV Schedule loads from  https://programtv.ru/ and parse using XMLTV Parser https://github.com/michabbb/xmltv-parser

Two options are available:
- installation like Production environment
- installation like Development environment

Flussonic always install and run like a service. 
Script for installation and configuration is provided

Portal is deploying and running in the docker container. At the same time web server (nginx) container runs.

System is managed by docker-compose with predefined configuration.
- *production.yml* for Production
- *docker-compose.yml* for Development

For Production env deploy and run pre build application image.

For Development env image is building at the moment of application container starting.

The master repository for source code and stable applicaton image is gitlab account.

**Gitlab CI process**

Each time when some one commit to master CI flow is running.

*Resulst of CI flow you can see here:* https://gitlab.com/andrey1611/fludemo/-/pipelines

CI flow consist of 3 stage:
1. Build. 
   On this stage we try to build an image and resolve all composer dependency .
   As a result of this stage we have a RC image.
2. Testing. 
   On this stage we perform deferent tests (unit test, feature test, ui test, etc).
   As a result of this test we are validate RC or failed the build.
3. Deploy. 
   On this stage we are copy of last vaildated RC to Container Registry as a lastest stable applicaton image.
   
** Testing **
- During CI Testing Phase we call phpunit
- Configuration of testsuites in the phpunit.xml
- We use Laravel testing capabilities so it's easy to create new Unit and Feature test
- CI performs test automatically after RC is built
- Run `sudo docker-compose -f production.yml exec "app" ./vendor/bin/phpunit` if you need to perform tests on Production (e.g. after last image was deployed) 

*Here I didn't develop special test suites for this application just present 2 examples.
The main idea: we have powerful testing framework and entrypoint for CI and deployed application.
So there is a lot of possibility how to extend it for particular purposes.*

**How to deploy to vscale.io virtual servers**

*Production environment*

- Initial installation:
    1. create directory for the project and navigate to it
    2. `wget https://gitlab.com/andrey1611/fludemo/-/raw/master/prod.configure.sh`
    3. `chmod +x prod.configure.sh && ./prod.configure.sh`
    4. `./install.prod.sh {flussonic server ip}`
    5. (optional, in case of you don't have working flussonic server yet) 
	
       `chmod +x install.flussonic.sh && ./install.flussonic.sh "{flussonic license}"`
- Deploy new builds:
    1. Go to project directory
    2. Run script `./update.prod.sh`

*Development environment*

- Initial installation:
    1. create directory for the project and navigate to it
    2. `git clone https://gitlab.com/andrey1611/fludemo.git .`
    3. `chmod +x install.dev.sh`
    4. `./install.dev.sh {flussonic server ip}` 
    5. (optional, in case of you don't have working flussonic server yet)
	
       `chmod +x install.flussonic.sh && ./install.flussonic.sh "{flussonic license}"`
- Run changed image on the server:

For most cases all you changes in the source code will be immediately reflected on your web server because you working directory is linked to image volume.

In case when you really need to rebuild image and run it again please perform:
    - `sudo docker-compose down`
    - `sudo docker-compose up -d --build`
	
If you want deploy your changes to Release just push your code to master.



 





