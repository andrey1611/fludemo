<?php

namespace App\Console;

use App\Console\Kernel;
use Illuminate\Support\Facades\DB;
use App\xmltvparser\parser;
use App\Channels;

class EpgConsole extends Kernel
{
    static $epgTimezone;
    static $epgUrl;
    static $epgFile;
    static $epgFileSuffix;
    static $epgDefaultCannel;

    public static function loadEpg() {
	self::initEnvVars();
	self::getRemouteFile();
	self::unZip('./'.self::$epgFile.self::$epgFileSuffix);

	$Parser = new parser();
	$Parser->setFile('./'.self::$epgFile);
	$Parser->setTargetTimeZone(env('EPG_TIMEZONE'));
	
	$channels = Channels::all();
	foreach($channels as $ch) {
	    $Parser->setChannelfilter($ch->channel);
	}

	try {
    	    $Parser->parse();
	} catch (Exception $e) {
    	    throw new \RuntimeException($e);
	}

	foreach($Parser->getEpgdata() as $row) {
    	    preg_match("/(\d+)\s+(\+\d+)/", $row['start_raw'], $matches);
	    DB::table('epg')->insertOrIgnore(
		[
		'start' => $row['start'], 
		'start_raw' => $matches[1],
		'timezone' => $matches[2],
		'channel' => $row['channel'],
		'stop' => $row['stop'],
		'title' => $row['title']
		]
	    );
    }

	unlink('./'.self::$epgFile.self::$epgFileSuffix);
	unlink('./'.self::$epgFile);
	return "Success!";
	//return print_r($Parser->getChannels(), true);
    }

    public static function getRemouteFile() {
	try {
	    copy(self::$epgUrl.self::$epgFile.self::$epgFileSuffix,'./'.self::$epgFile.self::$epgFileSuffix);
	} catch (Exception $e) {
    	    throw new \RuntimeException($e);
	}
    }

    public static function unZip($filename) {

        // Raising this value may increase performance
        $buffer_size = 4096; // read 4kb at a time
        $out_file_name = str_replace(self::$epgFileSuffix, '', $filename); 

        // Open our files (in binary mode)
        $file = gzopen($filename, 'rb');
	$out_file = fopen($out_file_name, 'wb'); 

	// Keep repeating until the end of the input file
	while (!gzeof($file)) {
	    // Read buffer-size bytes
	    // Both fwrite and gzread and binary-safe
	    fwrite($out_file, gzread($file, $buffer_size));
	}

	// Files are done, close files
	fclose($out_file);
	gzclose($file);
    }

    static function initEnvVars() {
	self::$epgTimezone      = env('EPG_TIMEZONE');
        self::$epgUrl 	     	= env('EPG_URL');
	self::$epgFile 	     	= env('EPG_FILE');
	self::$epgFileSuffix    = env('EPG_ARCH_FILE_SUFFIX');
	self::$epgDefaultCannel = env('EPG_DEFAULT_CHANNEL');
    }
}
