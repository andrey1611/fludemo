<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Channels;
use DateTime;
use DateTimeZone;

class EpgController extends Controller
{
    public function index() {
	return view('welcome',
	    array(
		'channel' => env('EPG_DEFAULT_CHANNEL'), 
		'Channels' => $this->getActiveChannels(),
		'urlStr' => 'ago=3600',
		'epgData' => $this->getEpgByChannel(env('EPG_DEFAULT_CHANNEL')),
		'fluUrl' => env('FLU_URL')
	    )
	);
    }

    public function indexWithId($id) {
	return view('welcome', 
	    array(
		'channel' => $id, 
		'Channels' => $this->getActiveChannels(),
		'urlStr' => 'ago=3600',
		'epgData' => $this->getEpgByChannel($id),
		'fluUrl' => env('FLU_URL')
	    )
	);
    }

    public function indexWithFrom($id, $from) {
	return view('welcome', 
	    array(
		'channel' => $id, 
		'Channels' => $this->getActiveChannels(),
		'urlStr' => "from=$from",
		'epgData' => $this->getEpgByChannel($id),
		'fluUrl' => env('FLU_URL')
	    )
	);
    }

    public function indexWithFromTo($id, $from, $to) {
	return view('welcome', 
	    array(
		'channel' => $id, 
		'Channels' => $this->getActiveChannels(),
		'urlStr' => "from=$from&to=$to",
		'epgData' => $this->getEpgByChannel($id),
		'fluUrl' => env('FLU_URL')
	    )
	);
    }


    function getActiveChannels() {
	return Channels::all();
    }

    function getEpgByChannel($chId) {
	$epgData =  DB::table('epg')->where('channel', $chId)->orderBy('start_raw', 'desc')->get();
	$currentDateSource = new DateTime();
	$returnEpg = array();

	foreach ($epgData as $epg) {
	    $startDateSource = new DateTime($epg->start, new DateTimeZone($epg->timezone));
	    $endDateSourse = new DateTime($epg->stop, new DateTimeZone($epg->timezone));

	    if($currentDateSource < $startDateSource) continue;

	    if($currentDateSource >= $startDateSource && $currentDateSource < $endDateSourse) {
		$returnEpg[] = [
		    "start"   => $startDateSource->format('Y-m-d H:i'),
		    "stop"    => $endDateSourse->format('H:i'),
		    "from"    => $startDateSource->getTimestamp(),
		    "channel" => $chId,
		    "title"   => $epg->title
		];
	    }
	    else {
		$returnEpg[] = [
		    "start"   => $startDateSource->format('Y-m-d H:i'),
		    "stop"    => $endDateSourse->format('H:i'),
		    "from"    => $startDateSource->getTimestamp(),
		    "to"      => $endDateSourse->getTimestamp(),
		    "channel" => $chId,
		    "title"   => $epg->title
		];
	    }
	}

	return $returnEpg;
    }
}
