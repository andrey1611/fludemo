<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEpgTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	if (!Schema::hasTable('epg')) {
    	    Schema::create('epg', function (Blueprint $table) {
        	$table->id();
		$table->text('start');
		$table->integer('start_raw');
		$table->text('timezone');
		$table->integer('channel');
        	$table->text('stop');
		$table->text('title');
		$table->timestamps();
		
		$table->unique(['channel', 'start_raw']);
    	    });
	}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('epg');
    }
}
