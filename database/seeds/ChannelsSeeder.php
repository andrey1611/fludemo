<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class ChannelsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
	DB::table('channels')->insertOrIgnore(['channel' => '146', 'title' => 'Первый']);
	DB::table('channels')->insertOrIgnore(['channel' => '317', 'title' => 'Россия 1']);
    }
}
