#!/bin/bash

[ -z "$1" ] && { >&2 echo "flussonic ip is required "; exit 0; }

touch ./database/database.sqlite
cp .env.example .env
echo "FLU_URL=$1:8080" >> .env

sudo docker-compose up -d --build
sudo docker-compose exec "app" composer install --no-interaction --ansi --no-suggest
sudo docker-compose exec "app" chown -R www-data:www-data storage
sudo docker-compose exec "app" php artisan down
sudo docker-compose exec "app" php artisan config:clear
sudo docker-compose exec "app" php artisan view:clear
sudo docker-compose exec "app" php artisan key:generate
sudo docker-compose exec "app" php artisan migrate
sudo docker-compose exec "app" php artisan db:seed --class=ChannelsSeeder
sudo docker-compose exec "app" php artisan loadepg
sudo docker-compose exec "app" php artisan up
