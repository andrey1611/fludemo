#!/bin/bash

[ -z "$1" ] && { >&2 echo "flussonic license is required"; exit 0; }

curl -sSf https://flussonic.com/public/install.sh | sudo sh
sudo cp ./docker/flussonic/flussonic.conf /etc/flussonic
[ ! -f "/etc/flussonic/license.txt" ] && { sudo touch /etc/flussonic/license.txt; }
sudo chmod a+w /etc/flussonic/license.txt
sudo echo "$1" > /etc/flussonic/license.txt
sudo /etc/init.d/flussonic start
