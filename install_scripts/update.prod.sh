#!/bin/sh

VOL_NAME=`sudo docker inspect app | jq ".[0].Mounts" | jq '.[] | select (.Type == "volume" and .Destination == "/var/www")' | jq '.Name' | sed s/\"//g`
CURR_IMAGE=`sudo docker inspect app | jq ".[0].Config.Image" | sed s/\"//g | cut -d: -f1`

sudo docker-compose -f production.yml down

sudo docker images | grep $CURR_IMAGE | grep -Po "\s([\d|a-f]{12})\s" | xargs sudo docker rmi
sudo docker volume rm $VOL_NAME

sudo docker-compose -f production.yml up -d
sudo docker-compose -f production.yml exec "app" composer install --no-interaction --ansi --no-suggest
sudo docker-compose -f production.yml exec "app" php artisan down
sudo docker-compose -f production.yml exec "app" php artisan config:clear
sudo docker-compose -f production.yml exec "app" php artisan view:clear
sudo docker-compose -f production.yml exec "app" php artisan migrate
sudo docker-compose -f production.yml exec "app" php artisan up
