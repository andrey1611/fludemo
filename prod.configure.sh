#!/bin/sh

wget https://gitlab.com/andrey1611/fludemo/-/raw/master/production.yml -Oproduction.yml
wget https://gitlab.com/andrey1611/fludemo/-/raw/master/install.flussonic.sh -Oinstall.flussonic.sh
wget https://gitlab.com/andrey1611/fludemo/-/archive/master/fludemo-master.tar.gz?path=docker -Odocker.tar.gz
wget https://gitlab.com/andrey1611/fludemo/-/archive/master/fludemo-master.tar.gz?path=install_scripts -Oinstall_scripts.tar.gz

tar -xzf docker.tar.gz
tar -xzf install_scripts.tar.gz

mv fludemo-master-install_scripts/install_scripts/ ./
mv fludemo-master-docker/docker/ ./

rm -r fludemo-master-docker fludemo-master-install_scripts
rm docker.tar.gz install_scripts.tar.gz

mv ./install_scripts/install.prod.sh ./
mv ./install_scripts/update.prod.sh ./

./install_scripts/git.install.sh
./install_scripts/docker.install.sh
./install_scripts/docker-compose.install.sh
./install_scripts/jq.install.sh

rm prod.configure.sh
