<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

	    ul li {
		font-size:8px;
	    }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
	<table width = "100%">
	    <tr>
		<td colspan="2" align = "center">
		<div class="links">
		    @foreach($Channels as $ch)
                	<a href="/{{$ch->channel}}">{{$ch->title}}</a>
		    @endforeach
                </div>
		</td>
	    </tr>

	    <tr>
		<td width = "40%">
		    <table with = "100%">
		    <tr><td style = "font-size:10px;">
			@foreach($epgData as $epg)
			    <tr><td style = "font-size:10px;">
			    @if (array_key_exists('to', $epg))
				<a href="/{{$channel}}/{{$epg['from']}}/{{$epg['to']}}">{{$epg['start']}} {{$epg['title']}}</a>
			    @else
				<a href="/{{$channel}}/{{$epg['from']}}">{{$epg['start']}} {{$epg['title']}}</a>
			    @endif
			    </td></tr>
			@endforeach
		    </table>
		</td>
		<td width = "60%" valign="top" align="right">
		    <iframe style="width:640px; height:480px;" allowfullscreen src="http://{!! $fluUrl !!}/ch{{$channel}}/embed.html?{!! $urlStr !!}"></iframe>
		</td>
	    </tr>
	</table>
    </body>
</html>
