<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'EpgController@index');
Route::get('/{id}', 'EpgController@indexWithId');
Route::get('/{id}/{from}', 'EpgController@indexWithFrom');
Route::get('/{id}/{from}/{to}', 'EpgController@indexWithFromTo');

//Route::get('test/{id}', 'EpgController@renderPage');

//Route::get('/{id}', 'EpgController@renderPage');
/*function () {
    return view('welcome');
});
*/

